import { Component } from "inferno";
import * as localforage from "localforage";
import { TabView } from "KaiUI";

type AppState = {
  tab: number;
}


class App extends Component<{}, AppState> {
  public state: AppState = { tab: 0 };

  constructor(props: {}) {
    super(props);

    this.state = {
      tab: 0,
    };
  }

  render() {
    const tab = this.state.tab;
  }
}
